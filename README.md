# FPGApedia companion repository

This repository collects historical documentation (datasheets, press releases, web sites content) related to FPGA vendors and its devices, used by the [FPGApedia](https://gitlab.com/rodrigomelo9/fpgapedia) project.

All the material in this repository is property of their respective owners.
To the best of our knowledge and belief, everything here have been publicly available and freely redistributed.
Please contact us if you have questions or concerns.
